export interface User {
  _id?: string,
  name: string,
  emails: string,
  password: string,
  cargo: string,
  createdAt?: string,
  updatedAt?: string
}