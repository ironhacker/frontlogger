import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { User } from '../models/user'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  API_LOCAL = `http://localhost:7400`
  API_CLOUD = `https://newloggerback.herokuapp.com`
  API_URL = `${this.API_CLOUD}/api`

  users!: User[];
  selectedUser: User = {
    name:"",
    emails:"",
    password:"",
    cargo:""
  }

  constructor(private http:HttpClient) {}

  getUsers(){
    return this.http.get<User[]>(this.API_URL)
  }

  createUser(user: User){
    return this.http.post(this.API_URL, user)
  }

  deleteUser(_id: string | undefined){
    console.log(_id)
    return this.http.delete(`${this.API_URL}/user/${_id}`)
  }

  putUser(user: User){
    return this.http.put(`${this.API_URL}/user/${user._id}`, user)
  }
  
  getUserLogged(){
    return this.http.get(`${this.API_URL}/nextwindow/60d531865bf519b092f38de1`)
  }

  getSingleLoggin(){
    return this.http.get(`${this.API_CLOUD}/login/google`)
  }
}
