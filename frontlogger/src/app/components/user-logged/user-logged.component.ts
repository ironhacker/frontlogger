import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'

@Component({
  selector: 'app-user-logged',
  templateUrl: './user-logged.component.html',
  styleUrls: ['./user-logged.component.scss']
})
export class UserLoggedComponent implements OnInit {

  userLogged = {
    name: '',
    thumbnail: ''
  } 

  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.getUserLogged()
  }

  getUserLogged(){
    console.log(`GETUSER EN COMPONENT LOGGED`)
    this.userService.getUserLogged().subscribe(
      res=>{
      console.log(res)
      return this.userLogged},
      err=>console.log(err)
    )
  }

  getSingleLoggin(){
    console.log(`GETUSER EN COMPONENT LOGGED`)
    this.userService.getSingleLoggin().subscribe(
      res=>{
      console.log(res)
      return this.userLogged},
      err=>console.log(err)
    )
  }
}
