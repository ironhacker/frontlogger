import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'
import { NgForm } from '@angular/forms'
import { User } from 'src/app/models/user';
import { formatCurrency } from '@angular/common';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers() {
    this.userService.getUsers().subscribe(
      res=>{
        this.userService.users=res
        console.log('ACtualizado')
      },
      err=>console.log(err)
    )
  }

  postUser(form: NgForm){
    this.userService.createUser(form.value).subscribe(
      res=>{
        this.getUsers()
        console.log('POST OK', res)
      },
      err=>console.log(err)
    )
    form.reset()
  }

  putUser(form: NgForm){
    this.userService.putUser(form.value).subscribe(
      res=>{
        this.getUsers()
        form.reset()
        console.log('PUT OK',res)
      },
      err=>console.log('THE ERROR WAS ',err)
    )
  }

  addUser(form: NgForm){
    form.value._id ? this.putUser(form) : this.postUser(form) 
  }

  deleteUser(_id: string | undefined){
    confirm('Are you sure?') ? this.userService.deleteUser(_id).subscribe(
      res=>{
        this.getUsers()
        console.log(res)},
      err=>console.log(err)
    ) : console.log('nononoono')
  }

  editUser(user: User){
    this.userService.selectedUser = user
  }

  resetForm(userForm: NgForm){
    console.log(userForm.value)
    userForm.reset()
  }

}
